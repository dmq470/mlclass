function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

count = size(theta);
X_n = X(:,2:count);
size(theta);
theta_n = theta(2:count);
	size(theta_n);
z_n = theta_n' * X_n';


% First feature only
X_1 = X(:,1);
z_1 = theta(1) * X_1';

z = theta' * X'; %'
temp = (log (sigmoid(z)) * (-y)) - (log(1 - sigmoid(z)) * (1 - y));
J = (1.0/m) * sum(temp) + ((lambda / (2 * m)) * sum(theta_n.^2))



%grad(1) = ((sigmoid(theta(1) * X_1'))' - y)' * X_1

% Features > 1
%temp2 = ((sigmoid(z_n))' - y)' * X_n ;
%temp2 = temp2 + (lambda * theta_n)';

temp2 = ((sigmoid(z))' - y)' * X
temp2(2:end) = temp2(2:end) + (lambda * theta_n)';


size(grad);
%grad(2:count,:) = temp2';
size(temp2')
size(grad)
grad = temp2 / m





% =============================================================

end
